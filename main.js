// I am changing this file

// load express library

const express = require('express');

// instance of express application 
var app = express();

// configure the routes
app.use(express.static(__dirname + "/public"));
// static does not allow you to escape the directory - it expects the root path

app.use("/images", express.static(__dirname + "/images"));


// check and match request

// specify the port 
var port = 3000;

// Bind the port 
app.listen(port, function(){
  console.log('Server on listening on port %d', port)
});
